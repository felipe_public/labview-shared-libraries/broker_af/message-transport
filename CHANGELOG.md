# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] 2020-12-23
### Added
- feat: sender name is sent with data for debugging purposes
- ci: included rules for not triggering pipeline in tags

### Changed
- ci: updated docker images for code quality and unit testing
- docs: removed badges from readme.md

## [1.1.0] 2020-08-27
### Added
- Possibility of forwarding timestamp, useful in broker usage.

## [1.0.0] 2020-08-07
### Added
- Interface for Message Transport.
- Snippets for Topic Usage.
- VIAn Cfg File.

### Changed
- Updated for LV2020.
- VI Analyzer performed.

### Removed
- Unused files that are not more compatible with Interfaces.

## [0.2.0] - 2020-04-28
### Added
- *Topic.lvlib*.
- Documentation: first step - UML Class Diagram.

### Changed
- Data Type dependency removed in libraries.json.
- Changed *Topic.lvclass* to a separate class.
- Folders Structure Updated to reflect project hierarchy.
- VI Documentation updated.

### Removed
- Payload Class. It was substituted by a generic LV Object Class.
- Header Type and Payload Class removed from libraries.json.

## [0.1.1] - 2020-03-23
### Added
- Changelog.md.

### Changed
- Readme.md updated.
- New version of *data-types.lvlib* - v0.5.0
- VI *Write Data to Message Transport.vim* to include *Return Data Type Class.vim* from Data Type.lvclass. This removes dependency message transport for data conversion.
- Include VI Documentation of the VIs.

## [0.1.0] - 2019-12-05
### Added
- Initial version.
