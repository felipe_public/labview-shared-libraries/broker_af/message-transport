# LabVIEW Message Transport Library
LabVIEW Core Library for Message Transport Class and Interface in Actor Framework Projects with Broker Library.

# Author
- Felipe Pinheiro Silva

## Installation
1. Includes the standard library (.lvlib) in your labview project or using SDM. 
- Provider Path: https://gitlab.com/felipe_public/labview-shared-libraries/broker_af

## Usage

### Message Transport Class Usage
![Message Usage](/docs/snippets/message transport usage.png)
### Topic Class Usage
![Topic Usage](/docs/snippets/topic usage.png)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
BSD3
